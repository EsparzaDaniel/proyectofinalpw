<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', function () {
    return view('home');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/foro', 'ForoController@forum')->name('Foro');

Route::get('/construcciones', 'ConstruccionesController@index')->name('Construcciones');

//Route::post('/construcciones', 'ConstruccionesController@store')->name('ConstruccionesStore');

Route::get('/juegos', 'JuegosController@games')->name('Juegos');

Route::get('/plataformas', 'PlataformasController@platforms')->name('Plataformas');

Route::get('/nuevaversion', 'NuevaVersionController@newversion')->name('Versión 1.17');

//Route::post('/foro', 'ForoController@store')->name('ForoStore');

Route::resource('construcciones', 'ConstruccionesController');
