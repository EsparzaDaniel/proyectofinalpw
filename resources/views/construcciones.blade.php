@extends('app')

@section('title', 'Minecraft | Publicar Construcción')
@section('id', 'construcciones')

@section('content')

<br><br>
<div class="container pt-4 pb-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Publicar Construcción</div>

                <div class="card-body">
                    <form action="{{ route('construcciones.store') }}" method="post" enctype="multipart/form-data">
                        @csrf

                            <div class="form-group row">
                              <label for="imagen" class="col-md-4 col-form-label text-md-right">Imagén</label>
                              <input type="file" class="form-control-file col-md-6" id="imagen" name="imagen" accept="image/*">
                              
                              @error('imagen')
                                <strong class="text-danger">{{$message}}</strong>
                              @enderror
                            </div>

                        <div class="form-group row">
                            <label for="titulo" class="col-md-4 col-form-label text-md-right">Titulo</label>

                            <div class="col-md-6">
                                <input id="titulo" type="text" class="form-control" name="titulo" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">Descripción</label>

                            <div class="col-md-6">
                                <textarea id="descripcion" type="text" class="form-control" name="descripcion" required autofocus></textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">Publicar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>

@endsection