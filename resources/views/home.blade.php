@extends('app')

@section('title', 'Minecraft')
@section('id', 'principal')

@section('content')

<!-- CONTENIDO -->

<section class="pb-4 pt-4">
    <div class="container pt-4" id="contenido">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                <p>Minecraft es un videojuego de construcción, de tipo mundo abierto o sandbox creado
                    originalmente por el sueco Markus Persson (conocido comúnmente como "Notch"),​ y posteriormente
                    desarrollado por su empresa, Mojang Studios. Fue lanzado públicamente el 17 de mayo de 2009, 
                    después de diversos cambios fue lanzada su versión completa el 18 de noviembre de 2011.
                </p>
            </div>
        </div>
    </div>

    <div class="container" id="galeria">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 align-items-center">
                <img class="img-fluid rounded mx-auto d-block" src="images/image1.jpg">
            </div>
        </div>
    </div>

    <div class="container pt-4 pb-2" id="contenido">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                <p>El 15 de septiembre de 2014, fue adquirido por la empresa Microsoft por un valor de 2500
                   millones USD. Este suceso provocó el alejamiento de Markus Persson de la compañía.
                </p>
            </div>
        </div>
    </div>

</section>

<section class="pt-4 pb-4">
    <div class="container ">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                <div class="card" style="width: 18rem;" id="tarjeta">
                    <img src="images/notch.jpg" class="card-img-top">
                    <div class="card-body">
                      <p class="card-text">Markus Alexej Persson (Notch) creador y programador de Minecraft y 
                          fundador de Mojang.</p>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>

<!-- END CONTENIDO -->

@endsection
