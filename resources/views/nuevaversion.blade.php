@extends('app')

@section('title', 'Minecraft | Versión 1.17')
@section('id', 'nuevaversion')

@section('content')
    
<!-- CONTENIDO -->

<section class="pb-2 pt-2">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-12">
           <h3 class="lead text-center titulo">Nueva Actualización 1.17</h3>
        </div>
      </div>
    </div>
</section>

  <section class="pb-4">
    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>La 1.17, primera salida de la actualización Caves & Cliffs, es una actualización mayor
                         próxima para la Java y Bedrock Edition planeada para publicarse a mediados del 2021. 
                         Fue anunciada durante la Minecraft Live 2020 el 3 de octubre de 2020.
                    </p>
                </div>
            </div>
        </div>
    </div>
  </section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 col-md-12 align-items-center">
                <img class="img-fluid rounded mx-auto d-block" src="images/1.17.jpg">
            </div>
        </div>
    </div>
</section>

<section class="pb-2 pt-4">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-lg-12">
               <h3 class="lead text-center titulo">Nuevos Bloques</h3>
            </div>
          </div>
        </div>
</section>

<section class="pb-2">
    <div class="container" id="cambios">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12">
                <img src="images/cobre.jpg">
            </div>
            <div class="col-lg-3 col-md-3 col-6">
                    <ul> 
                        <li type="disc">Hojas de azalea</li>
                        <li type="disc">Raices de azalea</li>
                        <li type="disc">Geoda de amatista</li>
                        <li type="disc">Cristales de amatista</li>
                        <li type="disc">Mena de cobre</li>
                        <li type="disc">Bloque de cobre</li>
                        <li type="disc">Escaleras de cobre</li>
                    </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-6">
                    <ul> 
                        <li type="disc">Losa de cobre</li>
                        <li type="disc">Planta de esporas</li>
                        <li type="disc">Dripleaf plant</li>
                        <li type="disc">Bloque de sculk</li>
                        <li type="disc">Vertedor de sculk</li>
                        <li type="disc">Brote de sculk</li>
                        <li type="disc">Espeleotema puntiagudo</li>
                    </ul>
            </div>
        </div>
    </div>
</section>

<section class="pb-2">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-12">
           <h3 class="lead text-center titulo">Nuevos Objetos</h3>
        </div>
      </div>
    </div>
</section>

<section class="pb-4">
<div class="container" id="cambios">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/amatista.jpg">
        </div>
        <div class="col-lg-6 col-md-6 col-12 ">
            <ul> <br><br>
                <li type="disc">Fragmentos de amatista</li>
                <li type="disc">Cubo con ajolote</li>
                <li type="disc">Saco</li>
                <li type="disc">Bayas luminosas</li>
                <li type="disc">Catalejo</li>
            </ul>
        </div>
    </div>
</div>
</section>

<section class="pb-2">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-12">
           <h3 class="lead text-center titulo">Nuevos Biomas</h3>
        </div>
      </div>
    </div>
</section>

<section class="pb-4">
<div class="container" id="cambios">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/lushcaves.jpg">
        </div>
        <div class="col-lg-6 col-md-6 col-12 ">
            <ul> <br><br><br>
                <li type="disc">Lush caves</li>
                <li type="disc">Dripstone caves</li>
                <li type="disc">Montañas nevadas</li>
            </ul>
        </div>
    </div>
</div>
</section>

<section class="pb-2">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-12">
           <h3 class="lead text-center titulo">Nuevos Mobs</h3>
        </div>
      </div>
    </div>
</section>

<section class="pb-4">
<div class="container" id="cambios">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/ajolote.jpg">
        </div>
        <div class="col-lg-6 col-md-6 col-12 ">
            <ul> <br><br><br>
                <li type="disc">Ajolote</li>
                <li type="disc">Warden</li>
                <li type="disc">Cabra</li>
            </ul>
        </div>
    </div>
</div>
</section>

<section class="pb-2">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-12">
           <h3 class="lead text-center titulo">Nuevas Texturas</h3>
        </div>
      </div>
    </div>
</section>

<section class="pb-4">
<div class="container" id="cambios">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/nuevosminerales.jpg">
        </div>
        <div class="col-lg-6 col-md-6 col-12 ">
            <ul> <br><br>
                <li type="disc">Mena de hierro</li>
                <li type="disc">Mena de oro</li>
                <li type="disc">Mena de redstone</li>
                <li type="disc">Mena de lapizlazuli</li>
                <li type="disc">Mena de esmeralda</li>
                <li type="disc">Mena de diamante</li>
            </ul>
        </div>
    </div>
</div>
</section>
<br><br>

<!-- END CONTENIDO -->

@endsection