<!-- HEADER -->

<header>
    <nav class="header navbar navbar-expand-lg navbar-dark fixed-top" id="cabecera">
        <div class="container">
        <a class="navbar-brand" href="{{URL::asset('/')}}">Minecraft</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
  
            <li class="nav-item">
              <a class="nav-link" href="{{URL::asset('/login')}}">Foro</a>
            </li>
  
            <li class="nav-item">
              <a class="nav-link" href="{{URL::asset('/juegos')}}">Juegos</a>
            </li>
  
            <li class="nav-item">
              <a class="nav-link" href="{{URL::asset('/plataformas')}}">Plataformas</a>
            </li>
  
            <li class="nav-item">
              <a class="nav-link" href="{{URL::asset('/nuevaversion')}}">Versión 1.17</a>
            </li>
  
          </ul>
        </div>
         </div>
      </nav>
  </header>
  
  <!-- END HEADER -->