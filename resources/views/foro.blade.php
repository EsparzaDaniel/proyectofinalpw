@extends('app')

@section('title', 'Minecraft | Foro')
@section('id', 'foro')

@section('content')

<!-- CONTENIDO -->
<section>
  <div class="container pt-2">
      <div class="row">
          <div class="col text-right">
            <a href="{{ route('construcciones.index') }}" class="btn btn-secondary">Publicar Construcción</a>
          </div>
      </div>
  </div>
</section>

<section class="pb-4 pt-4">
    <div class="container pt-4 pb-4" id="contenido">
        <div class="row">

            @foreach ($builts as $built)
            <div class="col-12 col-lg-6 col-md-6 col-sm-12 pb-4">
              <div class="card d-block mx-auto" style="width: 18rem;">
                  <img src="{{ $built->imagen }}" class="card-img-top" id="tarjeta">
                  <div class="card-body">
                    <h5 class="card-title">{{ $built->titulo }}</h5>
                    <p class="card-text">{{ $built->descripcion }}</p>
                  </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>

<!-- END CONTENIDO -->

@endsection
