@extends('app')

@section('title', 'Minecraft | Plataformas')
@section('id', 'plataformas')

@section('content')
    
    <!-- CONTENIDO -->
<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/javaedition.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Java Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel9.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Java Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel10.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Java Edition</h5>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>La edición original de Minecraft, ahora conocida como Java Edition, se desarrolló por
                       primera vez en mayo de 2009. Persson lanzó un video de prueba en YouTube de una versión
                       anterior de Minecraft. Persson completó el programa base de Minecraft durante un fin de
                       semana de ese mes y se lanzó una prueba privada en TigIRC el 16 de mayo de 2009. 
                       El juego se lanzó al público por primera vez el 17 de mayo de 2009 como una versión de
                       desarrollo en los foros de TIGSource. Persson actualizó el juego basándose en los 
                       comentarios de los foros. Esta versión más tarde se conocerá como la versión clásica. 
                       Otras fases de desarrollo denominadas Survival Test, Indev e Infdev se lanzaron entre 
                       septiembre de 2009 y junio de 2010.
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/bedrockedition.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Bedrock Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel11.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Bedrock Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel12.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Bedrock Edition</h5>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>Minecraft Bedrock (anteriormente Minecraft: Pocket Edition, Minecraft: Windows 10 Edition,
                         Minecraft: PS Edition o Minecraft: Xbox One Edition), es una edición de Minecraft para 
                         dispositivos móviles, realidad virtual, consolas de videojuegos y la Microsoft Store de
                          Windows 10. Es la contraparte de las ediciones Minecraft: Java Edition, destinada para 
                          computadoras, Minecraft: Legacy, versión descontinuada para Wi U, Nintendo, entre otras
                           consolas y de Minecraft: Education Edition, destinada para la enseñanza. <br>
 
                        Esta edición está basada en el motor Bedrock Engine y fue desarrollada para
                         unificar las diferentes versiones del juego existentes previamente para cada consola de
                          videojuegos y dispositivos móviles y permitir la interacción entre estas plataformas. 
                          Consta de versiones para Android, Xbox One, ios, Nintendo Switch, Apple TV, Fire TV, 
                          Oculus Rift, Gear VR y dispositivos basados en el sistema operativo Windows 10, incluyendo
                           computadoras de escritorio, dispositivos móviles y HoloLens.​
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- END CONTENIDO -->

@endsection