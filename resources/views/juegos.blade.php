@extends('app')

@section('title', 'Minecraft | Juegos')
@section('id', 'juegos')

@section('content')
    
    <!-- CONTENIDO -->
<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/image1.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel1.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft</h5>
                          <p>Diferentes biomas</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel2.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft</h5>
                          <p>Cuevas para explorar</p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>Minecraft es un juego de mundo abierto, por lo que no posee un objetivo específico, 
                        permitiéndole al jugador una gran libertad en cuanto a la elección de su forma de jugar.
                        A pesar de ello, el juego posee un sistema de logros.​ El modo de juego predeterminado 
                        es en primera persona, aunque los jugadores tienen la posibilidad de cambiarlo a tercera 
                        persona.​ El juego se centra en la colocación y destrucción de bloques, siendo que este se 
                        compone de objetos tridimensionales cúbicos, colocados sobre un patrón de rejilla fija.
                        Estos cubos o bloques representan principalmente distintos elementos de la naturaleza, 
                        como tierra, piedra, minerales, troncos, entre otros. Los jugadores son libres de 
                        desplazarse por su entorno y modificarlo mediante la creación, recolección y transporte
                        de los bloques que componen al juego, los cuales solo pueden ser colocados respetando la 
                        rejilla fija del juego. ​Los jugadores crean granjas que son para conseguir un determinado
                        material más fácil por ejemplo una granja de oro. En el juego se pueden encontrar 
                        estructuras especiales como aldeas, galerías mineras, templos marinos, pirámides y 
                        templos selváticos.
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/dungeons.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Dungeons</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel3.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Dungeons</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel4.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Dungeons</h5>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>A diferencia de Minecraft, Minecraft Dungeons no tiene artesanía, construcción o la 
                        capacidad de destruir bloques. En cambio, se centra en ser un juego de rastreo de mazmorras,
                        donde el jugador explora mazmorras generadas aleatoriamente llenas de monstruos generados
                        aleatoriamente, trata con trampas y rompecabezas, y encuentra tesoros aleatorios.​ 
                        No hay un sistema de clases. Un jugador puede usar cualquier arma o armadura que desee.
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/educationedition.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Education Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel5.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Education Edition</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel6.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Education Edition</h5>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>Minecraft Education Edition es una versión educativa del juego base, diseñada específicamente
                         para su uso en establecimientos educativos como escuelas, y construida a partir del código
                          base de Bedrock. Está disponible en Windows 10, MacOS, iPadOS y Chrome OS. Incluye un paquete
                           de recursos de química, planes de lecciones gratuitos en el sitio web de Minecraft: 
                           Education Edition y dos aplicaciones complementarias gratuitas: Code Connection y Classroom
                            Mode. <br>
 
                        Se llevó a cabo una prueba beta inicial entre el 9 de junio y el 1 de noviembre de 2016. 
                        El juego completo se lanzó en Windows 10 y MacOS el 1 de noviembre de 2016. El 20 de agosto 
                        de 2018, Mojang Studios anunció que llevaría Education Edition a iPadOS en otoño de 2018. Se 
                        lanzó a la App Store el 6 de septiembre de 2018. El 27 de marzo de 2019, se anunció que JD.com
                         administraría la Edición Educativa en China. El 26 de junio de 2020, se puso a disposición una 
                         versión beta pública de Education Edition para los Chromebooks compatibles con Google Play Store. El juego completo se lanzó en Google Play Store para Chromebooks el 7 de agosto de 2020.
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="images/earth.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Earth</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel7.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Earth</h5>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <img src="images/carrusel8.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Minecraft Earth</h5>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container pt-4" id="contenido">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-sm-12">
                    <p>Minecraft Education Edition es una versión educativa del juego base, diseñada específicamente
                         para su uso en establecimientos educativos como escuelas, y construida a partir del código
                          base de Bedrock. Está disponible en Windows 10, MacOS, iPadOS y Chrome OS. Incluye un paquete
                           de recursos de química, planes de lecciones gratuitos en el sitio web de Minecraft: 
                           Education Edition y dos aplicaciones complementarias gratuitas: Code Connection y Classroom
                            Mode. <br>
 
                        Se llevó a cabo una prueba beta inicial entre el 9 de junio y el 1 de noviembre de 2016. 
                        El juego completo se lanzó en Windows 10 y MacOS el 1 de noviembre de 2016. El 20 de agosto 
                        de 2018, Mojang Studios anunció que llevaría Education Edition a iPadOS en otoño de 2018. Se 
                        lanzó a la App Store el 6 de septiembre de 2018. El 27 de marzo de 2019, se anunció que JD.com
                         administraría la Edición Educativa en China. El 26 de junio de 2020, se puso a disposición una 
                         versión beta pública de Education Edition para los Chromebooks compatibles con Google Play Store. El juego completo se lanzó en Google Play Store para Chromebooks el 7 de agosto de 2020.
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- END CONTENIDO -->

@endsection