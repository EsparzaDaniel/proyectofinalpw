$(function(){
    $('[data-toggle="tooltip"]').tooltip()
})

//Smooth scrolling

document.querySelectorAll('a[href^="#"]').forEach((anchor) => {

    anchor.addEventListener("click", function (e) {

        e.preventDefault();

        document.querySelector(this.getAttribute("href")).scrollIntoView({

            behavior: "smooth",
        });

    });
});

$(window).scroll(function()
{
    if ($("#cabecera").offset().top > 80)
    {
        $("#cabecera").addClass("header-scroll");
    }
    else
    {
        $("#cabecera").removeClass("header-scroll");
    }
})