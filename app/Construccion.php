<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Construccion extends Model
{
    protected $table = 'construcciones';

    protected $fillable = [
        'titulo', 'descripcion', 'imagen',
    ];
}
