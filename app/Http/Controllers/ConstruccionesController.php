<?php

namespace App\Http\Controllers;

use App\Construccion;
use auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ConstruccionesController extends Controller
{
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('construcciones');
    }

    public function store(Request $request)
    {
       //dd($request->all());
       $request->validate([
           'imagen' => 'required|image|max:800'
       ]);

        $construccion = new Construccion;

        $request->file('imagen')->store('public');
        $imagen = $request->file('imagen')->store('public');
        $url = Storage::url($imagen);

        $construccion->user_id = Auth::user()->id;
        $construccion->titulo = $request->input('titulo');
        $construccion->descripcion = $request->input('descripcion');
        $construccion->imagen = $url;
        $construccion->save();

        return redirect('/foro');
    }
}
