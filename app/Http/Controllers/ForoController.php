<?php

namespace App\Http\Controllers;

use App\Construccion;
use Illuminate\Http\Request;

class ForoController extends Controller
{
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function forum()
    {
        $construcciones = \DB::table('construcciones')
                 ->select('construcciones.*')
                 ->orderBy('id', 'DESC')
                 ->get();
        return view('foro', ['builts' => $construcciones]);
    }

    public function store(Request $request)
    {
    }
}
