<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NuevaVersionController extends Controller
{
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function newversion()
    {
        return view('nuevaversion');
    }
}
