<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JuegosController extends Controller
{
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function games()
    {
        return view('juegos');
    }
}
