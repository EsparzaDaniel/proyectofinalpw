<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlataformasController extends Controller
{
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function platforms()
    {
        return view('plataformas');
    }
}
